#include <stdio.h>
#include <stdlib.h>

void parkingInit(char *, char *);
int muestraMenu();
void parkingCar(char *, char *);
void removeCar (char *, char *);
void printPark (char *, char *);

void parkingInit(char *C, char *G){
    int i;
    
    for (i = 0; i < 14; i++){
        C[i] = '|';
    }
    for (i = 0; i < 10; i++){
        G[i] = '|';
    }
}
int muestraMenu (){
    
    int choose;
    
    printf ("\nEstacionamiento\n\nMENU:\n\n");
    printf ("\t1.- Estacionar Coche\n");
    printf ("\t2.- Retirar Coche\n");
    printf ("\t3.- Mostrar situacion del Estacionamiento\n");
    printf ("\t4.- Salir\n\n");
    printf ("\t\tOpcion: "); scanf ("%d", &choose);
    
    return choose; 
    
}

void parkingCar (char *C, char *G){
     int i, x;
     char t;
     x = 0;
     fflush (stdin);
     printf("\nTama�o del coche(c/g): "); scanf ("%c", &t);
     fflush (stdin);
     if (t == 'c' || t == 'C'){
           for (i = 0; i < 14; i++){
               if (C[i] == '|'){
                  C[i] = 'C';
                  x = 1;
                  break;
               }
           }
           if (x == 0){
                 for (i = 0; i < 10; i++){
                     if (G[i] == '|'){
                        G[i] = 'G';
                        x = 1;
                        break;
                     }
                 }
           }
     }
     if (t == 'g' || t == 'G'){
           for (i = 0; i < 10; i++){
               if (G[i] == '|'){
                  G[i] = 'G';
                  x = 1;
                  break;
               }
           }
     }
     if (x == 1)
        printf ("\nEl coche se estaciono correctamente\n\n");
     else
         printf ("\nYa no hay espacios disponibles\n\n");
     system("PAUSE");
}

void removeCar (char *C, char *G){
     int p, i, x;
     char t;
     x = 0;
     printf ("\nEn que lugar esta? ((C/G),posicion): ");
     scanf ("%c,%d", &t, &p);
     if (t == 67 || t == 99){
           if (C[p-1] == 'C'){
              C[p-1] = '|';
              x = 1;
              
           }
     }
     if (t == 71 || t == 103){
           if (G[p-1] == 'C' || G[p-1] == 'G'){
                    G[p-1] = '|';
                    x = 1;
                    
           }  
     }
     if (x == 1)
        printf ("\nEl coche se retiro correctamente\n\n");
     else
         printf ("\nNo hay ningun coche en esta posicion\n\n");
     system("PAUSE");
}

void printPark (char *C, char *G){
     int i;
     printf ("\nLugares Compactos: \n");
     for (i = 0; i < 14; i++){
         printf ("  %d", i);
     }
     printf ("\n");
     for (i = 0; i < 14; i++){
         printf ("  %c", C[i]);
     }
     printf ("\n\nLugares Grandes: \n");
     for (i = 0; i < 10; i++){
         printf ("  %d", i);
     }
     printf ("\n");
     for (i = 0; i < 10; i++){
         printf ("  %c", G[i]);
     }
     printf ("\n\n");
     system("PAUSE");
}

int main(){
    int c;
    char parkingC [14];
    char parkingG [10];
    parkingInit(parkingC, parkingG);
    while (c != 4){
          c = muestraMenu();
          switch (c){
           case 1:
               parkingCar(parkingC, parkingG);
               system ("CLS"); 
               break;
           case 2:
                removeCar(parkingC, parkingG);
                system ("CLS");
                break;
           case 3:
                printPark (parkingC, parkingG);
                system ("CLS");
                break;
           default:
                   printf ("Opcion invalida");
                   system ("CLS");
                   muestraMenu ();
           }
    }
               
    system("PAUSE");	
    return 0;
}
